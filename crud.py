# crud com lista de 10 elementos ordenados


# 1 - criar lista, retornando o que foi criado


def criarLista():
    lista = []

    start = 0

    stop = 10

    step = 1

    lista = list(range(start, stop, step))

    # print(lista)

    return lista


# 2 - método para exibir um menu com as opcoes de crud (operacoes) para o usuario escolher


def exibirMenu():
    print("-------------- DIGITE A OPÇAO DESEJADA -------------------")

    print("[I]nserir elemento \n" +

          "[M]ostrar lista \n" +

          "[R]emover elemento \n" +

          "[A]lterar lista \n" +

          "[S]air")

    opcao = input("Digite a opção desejada: ")

    return opcao


# 3 - método para inserir um elemento na lista

def inserir(lista, elemento):
    lista.append(elemento)

    return lista  # devolvendo a lista atualizada, a lista anterior com o NOVO elemento


# 4 - método para deletar um elemento da lista

def remover(lista, elemento):
    lista.pop(elemento)

    return lista


### ---- MAIN ---- ####

listaCrud = []

opcao = 0

listaCrud = criarLista()

print(f"Lista Original: {listaCrud}")

while (True):

    opcao = exibirMenu()

    if (opcao == 'i'):

        # inserir

        print("### Método de inserção selecionado")

        elemento = int(input("Digite o elemento a ser inserido: "))

        listaCrud = inserir(listaCrud, elemento)

        print("--------> O elemento %d foi adiconado com sucesso" % elemento)

    elif (opcao == 'r'):

        # remover

        print(f"Lista Atual: {listaCrud}")

        index = int(input("Digite a posição do elemento a ser removido: "))

        listaCrud = remover(listaCrud, index)

    if (opcao == 's'):
        break

print(f"Lista Original: {listaCrud}")

###-----------------------------------------------------------------####


# PRIMEIRA VERSAO - CRIE A LISTA GLOBAL

# inserir

# entradas: lista, elemento

# saida: lista

# mostrar

# remover

# alterar

# exibir Menu


# [I]nserir

# [M]ostrar

# [R]emover

# [A]lterar

# [S]air


'''outra alternativa para remover 


    print(todaLista)


    x=input('Qual elemento quer alterar? ')


    y=todaLista.index(x)


    z=input('Qual o novo valor? ')


    todaLista[y]=z


    print('nova lista',todaLista)


'''
