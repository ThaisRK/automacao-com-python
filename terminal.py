from datetime import *
import os
import pathlib

data_hora = datetime.now()
data_formatada = ("%02d/%02d/%04d" %
                  (data_hora.day, data_hora.month, data_hora.year))
hora_formatada = ("%d:%d" % (data_hora.hour, data_hora.minute))
senha = "ads"
usuario = "thais"

while True:
    print("#### Seja Bem Vindo ####")
    usuario_input = input("Usuario: ")
    senha_input = input("Senha: ")

    while senha != senha_input or usuario != usuario_input:
        print("Usuário ou Senha Incorreta!")
        print("---------------------------")
        print("##### Seja Bem Vindo #####")
        usuario_input = input("Usuario: ")
        senha_input = input("Senha: ")

    print("--> Login realizado com sucessso em", data_formatada, hora_formatada)
    print("...Carregando Didetório...")
    print(" ")

    os_dir = os.getcwd()
    lista_dir = os.listdir()
    qnt_arquivos = len(lista_dir)
    # --- UTILIZANDO MANUPILAÇÃO DE LISTA E STRING
    # diretorios = os_dir.split("/")
    # print("Diretorio atual = ", os_dir)
    # print("Quantidade de arquivos = ", qnt_arquivos )
    # print("Arquivos = ", lista_dir)
    # print("Unidade do disco = ", os_dir[0])
    # print("Nome do usuario = ", usuario)
    # print("Pasta atual = ", diretorios.pop() )

    # --- UTILIZANDO BIBLIOTECA pathlib
    purePath_dir = pathlib.PurePath(os_dir)
    print("Diretorio atual = ", purePath_dir)
    print("Quantidade de arquivos = ", qnt_arquivos)
    print("Arquivos = ", lista_dir)
    print("Unidade do disco = ", purePath_dir.root)
    print("Nome do usuario = ", usuario)
    print("Pasta atual = ", purePath_dir.name)

    print("##### FIM DA OPERAÇÃO #####")
    print("--> LOGOUT realizado  em", data_formatada, hora_formatada)
    print(" ")