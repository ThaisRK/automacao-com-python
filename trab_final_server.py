from socket import *
import csv
import os
import platform
from datetime import *
from subprocess import PIPE, Popen

myHost = 'localhost'
myPort = 50007

socketServer = socket(AF_INET,SOCK_STREAM) #combinacao = Server TCP/IP
socketServer.bind((myHost, myPort))

senha = "ads"
usuario = "thais"

def auth():
    usuario_input = connection.recv(1024).decode("utf-8")
    print("Usuario entrando: ", usuario_input)
    senha_input = connection.recv(1024).decode("utf-8")
    print("Sennha entrando: ", senha_input)
    while senha != senha_input or usuario != usuario_input:
        failedAccess = "Usuário ou Senha Incorreta!"
        connection.send(failedAccess.encode("utf-8"))
        # getUserInput()
        usuario_input = connection.recv(1024).decode("utf-8")
        print("Usuario entrando: ", usuario_input)
        senha_input = connection.recv(1024).decode("utf-8")
        print("Sennha entrando: ", senha_input)
    successfulAccess = "Login realizado com sucessso"
    connection.send(successfulAccess.encode("utf-8"))

def getConnection():
    global connection
    socketServer.listen(1)
    print("Servidor Escutando...")
    connection, id = socketServer.accept()

def ping(host):
    parameters_ping_m = "-c 4"
    parameters_ping_w = "-n 1"
    parameters_ping_ipv4 = " -4"
    if platform.system().lower() == "windows":
        ping_str = parameters_ping_w
    else:
        ping_str = parameters_ping_m
    cmd = "ping " + ping_str + " " + host + parameters_ping_ipv4
    p = Popen(args=cmd, stdout=PIPE, shell=True)
    for line in p.stdout:
        decoded = line.decode('utf-8').splitlines()
        for lineD in decoded:
            print(lineD)
    p.wait()
    get_data(p, host)
    return p

def get_data(ping_response, host):
    host_url = getfqdn(host)
    host_ip = (gethostbyname(host))

    poll = ping_response.poll()
    if poll == 0:
        status = "ATIVO"
    else:
        status = "INATIVO"
    date_time = datetime.now()
    date_format = ("%02d/%02d/%04d" %
                   (date_time.day, date_time.month, date_time.year))
    time_format = ("%d:%d" % (date_time.hour, date_time.minute))
    createCSV(host_url, host_ip, status, date_format, time_format)

def createCSV(host_url, host_ip, status, date_format, time_format):
    header = ["URL", "IP", "STATUS", "DATA", "HORA"]
    with open('logips.csv', 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=header)
        file_empty = os.stat(file.name).st_size == 0
        if file_empty:
            writer.writeheader()
        writer.writerow(
            {'URL': host_url,
             'IP': host_ip,
             'STATUS': status,
             'DATA': date_format,
             'HORA': time_format})

def imprime():
    file = open("logips.csv", "r")
    log = file.read()
    file_dir = os.path.abspath(file.name)

    print("\n##############################################\n"
          "## Todas URL's foram exacutadas com sucesso ##\n"
          "## Gerando log de resultados                ##\n"
          "## Log gerado com sucesso                   ##\n"
          "##############################################\n \n"
          ">>>>> DIRETÓRIO DO LOG: " + file_dir + "\n")
    # print(log)
    return log

while True:
    getConnection()
    auth()
    clienteOn = connection.recv(1024).decode("utf-8")
    while clienteOn=="true":

        ##############################################
        #2 recieve option
        option = connection.recv(1024).decode("utf-8")
        #3 send option confirmed
        connection.send(option.encode("utf-8"))
        resp = ''
        if option=='I':
            os_dir = os.getcwd()
            resp = os_dir
            connection.send(resp.encode("utf-8"))
        if option=='P':
            host = connection.recv(1024).decode("utf-8")
            ping(host)
        if option=='S':
            connection.close()

        print("terminou rodada do servidor")



    connection.close()
