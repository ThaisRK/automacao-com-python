from socket import *
from datetime import *

# import socketserver
#
#
# from sympy import as_finite_diff

#nome host
myHost = 'localhost'

#porta
myPort = 50007

#Etapas
#1. Criar um socket - objeto
#Duas constantes sao necessarias:
#1. Familia do endereco (AF_INET)
#2. Indicar se o stream da conversa eh padrao ou datagram: padrao SOCK_STREAM
    # Padrão: SOCK_STREAM --> utilizado
    # Datragram (UDP): SOCK_DGRAM

#Configuracoes necessarias:
# Protocolo IP - AF_INET
# Protocolo da camada de transporte (TCP - UDP): SOCK_STREAM


### AGORA PODEMOS CRIAR O NOSSO SOCKET SERVER (QUE EH UM OBJETO)
socketServer = socket(AF_INET,SOCK_STREAM) #combinacao = Server TCP/IP


##Depois de criado e configurado o Socket do Servidor,
# precisa vincular ele com porta e host
socketServer.bind((myHost, myPort))

date_time = datetime.now()
date_format = ("%02d/%02d/%04d" %
               (date_time.day, date_time.month, date_time.year))
counter = 0
while True:
    counter=counter+1
    # Socket server criado, socket vinculado a um host e a uma porta
    # Socket apto para comecar a escutar
    socketServer.listen(1)

    #Na escuta
    print("Servidor Escutando...")
    print("Data:", date_format)

    #Aceita as solicitacoes?!
    #se sim, retorna o -id- do cliente que pediu a conexao
    connection, id = socketServer.accept()
    #verificando com quem o servidor se conectou
    identifiers = str(id)[1:-1].split(',')
    identifier_id = identifiers[0]
    identifier_ip = identifiers[1]
    print("ID: ", identifier_id)
    print("IP: ", identifier_ip)

    ##neste momento ja temos um cliente em conexao com ele
    # recebe msg enviada pelo cliente - primeira vez
    msg = connection.recv(1024)

    #recebeu msg do cliente
    # print("Servidor diz: Mensagem do cliente recebida com sucesso")

    #Papel do servidor aqui: dar um 'echo' na msg que recebe do cliente
    #enviar a msg com o echo para o cliente
    print("Cliente conectado: ", counter)
    connection.send(b'Echo =>' + msg)
    # print("Servidor diz: Solicitacao atendida!")
    print("---------------------------------")
    print(" ")


    #fecha conexao
    connection.close()




