from socket import *
import os

serverHost = "localhost"
serverPort = 50007

socketClient = socket(AF_INET, SOCK_STREAM)
socketClient.connect((serverHost, serverPort))
print('Cliente solicitando conexão com servidor')

failedAuth = "Usuário ou Senha Incorreta!"
msgServidorAuthDecoded=''

def auth():
    global msgServidorAuthDecoded
    print("#### Seja Bem Vindo ####")
    usuario_input = input("Usuario: ")
    socketClient.send(usuario_input.encode("utf-8"))
    senha_input = input("Senha: ")
    socketClient.send(senha_input.encode("utf-8"))
    msgServidorAuth = socketClient.recv(1024)
    msgServidorAuthDecoded = msgServidorAuth.decode("utf-8")
    print("Servidor disse: ", msgServidorAuthDecoded)


def imprime():
    file = open("logips.csv", "r")
    log = file.read()
    file_dir = os.path.abspath(file.name)

    print("\n##############################################\n"
          "## Todas URL's foram exacutadas com sucesso ##\n"
          "## Gerando log de resultados                ##\n"
          "## Log gerado com sucesso                   ##\n"
          "##############################################\n \n"
          ">>>>> DIRETÓRIO DO LOG: " + file_dir + "\n")
    # print(log)
    return log

while True:
    while msgServidorAuthDecoded==failedAuth or msgServidorAuthDecoded=='':
        auth()
    clienteOn = "true"
    socketClient.send(clienteOn.encode("utf-8"))

    while clienteOn=="true":

        print("############# MENU #############")
        print("[I]Informações do Diretório Atual \n" +
              "[P]Ping\n" +
              "[S]air")
        print("################################")
        option = input(">>> Digite a opção desejada: ")

        ##############################################
        #1 send option
        socketClient.send(option.encode("utf-8"))
        #4 recieve option confirmed
        optionConfirmed = socketClient.recv(1024).decode("utf-8")
        if optionConfirmed=='I':
            resp = socketClient.recv(1024).decode("utf-8")
            print(">>> Seu diretório atual é: "+resp)
        if optionConfirmed=='P':
            print("entrou nometodo sendHost")
            host = input(">>> Digite o IP ou Endereco: ")
            print(host)
            socketClient.send(host.encode("utf-8"))
            print(imprime())
        if optionConfirmed=='S':
            break
        print("terminou rodada do cliente")



    socketClient.close()


