'''nome = input("Digite o nome: ")
sobrenome = input("Digite o sobrenome: ")
idade = str(input("Digite a idade: "))
print("O nome é: %s %s e tem %s anos" % (nome, sobrenome, idade))'''

'''nome = input("Digite o nome: ")
sobrenome1 = input("Digite o sobrenome: ")
sobrenome2 = input("Digite o segundo sobrenome: ")
print(" %s, %s. %s." % (sobrenome2, nome[0], sobrenome1[0]))'''

'''nome = input("Digite o nome completo: ")
splitted = nome.split(' ')
nome = splitted[0]
sobrenome1 = splitted[1]
sobrenome2 = splitted[2]
print(" %s, %s. %s." % (sobrenome2, nome[0], sobrenome1[0]))'''

'''directory = "hoMe"
print(directory[3-1])'''

'''alfabeto = ("a", "b", "c", "d", "e", "f", "g", "h")
# fatia = slice(0, 8, 3) #start, end, step
fatia = slice(0, 8, 3) #start, end, step
print(alfabeto[fatia])'''

'''import os
currentDir = os.getcwd()
currentDir = str(currentDir)
directories = currentDir.split('/')
print("Current Dir: ", currentDir)
print("Diretorios", directories)'''

from datetime import *
import os
import pathlib

data_hora = datetime.now()
data_formatada = ("%02d/%02d/%04d" %
                  (data_hora.day, data_hora.month, data_hora.year))
hora_formatada = ("%d:%d" % (data_hora.hour, data_hora.minute))
senha = "ads"
usuario = "thais"

while True:
    print("#### Seja Bem Vindo ####")
    usuario_input = input("Usuario: ")
    senha_input = input("Senha: ")

    while senha != senha_input or usuario != usuario_input :
        print("Usuário ou Senha Incorreta!")
        print("---------------------------")
        print("##### Seja Bem Vindo #####")
        usuario_input = input("Usuario: ")
        senha_input = input("Senha: ")

    print("--> Login realizado com sucessso em", data_formatada, hora_formatada)
    print("...Carregando Didetório...")

    os_dir = os.getcwd()
    # --- UTILIZANDO MANUPILAÇÃO DE LISTA E STRING
    lista_dir = os.listdir()
    qnt_arquivos = len(lista_dir)
    # diretorios = os_dir.split("/")
    # print("Diretorio atual = ", os_dir)
    # print("Quantidade de arquivos = ", qnt_arquivos )
    # print("Arquivos = ", lista_dir)
    # print("Unidade do disco = ", os_dir[0])
    # print("Nome do usuario = ", usuario)
    # print("Pasta atual = ", diretorios.pop() )

    # --- UTILIZANDO BIBLIOTECA pathlib
    purePath_dir = pathlib.PurePath(os_dir)
    print("Diretorio atual = ", purePath_dir)
    print("Quantidade de arquivos = ", qnt_arquivos )
    print("Arquivos = ", lista_dir)
    print("Unidade do disco = ", purePath_dir.root)
    print("Nome do usuario = ", usuario)
    print("Pasta atual = ", purePath_dir.name )

    break


# --------------------------------------
# purePath_dir = pathlib.PurePath(os_dir)
# print("purePath_dir: " ,purePath_dir)
#
# partes = purePath_dir.parts
# print("purePath_dir partes: " ,partes)
#
# corrente = purePath_dir.name
# print("purePath_dir corrente: " ,corrente)

# ud = purePath_dir.root
# print("ud: ", ud)
