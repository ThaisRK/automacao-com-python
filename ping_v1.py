import os
import platform

while True:

    def ping(host):
        parametrosPingM = "-c 4"
        parametrosPingW = "-n 1"
        if (platform.system().lower()=="windows"):
            ping_str = parametrosPingW
        else:
            ping_str = parametrosPingM

        linhaExecucao = os.system("ping " + ping_str + " " + host )
        retornoPing = linhaExecucao

        return retornoPing

    host = input("Digite o ip ou Endereco: ")
    print("--- Comecando a pingar: ", host)
    ping(host)

    continuar = input("Deseja continuar? ")
    if (continuar!="s"):
        break