#### PING EXEMPLO 1

import os
import platform

# def ping(host):
#     print("ping " + host)
#     parametrosPing = "-c 4"
#     linhaExecucao = os.system("ping " + parametrosPing + " " + host)
#     retornoPing = linhaExecucao
#     return retornoPing

def ping(host):
    parametrosPingM = "-c 4"
    parametrosPingW = "-n 1"
    if (platform.system().lower()=="windows"):
        ping_str = parametrosPingW
    else:
        ping_str = parametrosPingM

    print("ping " + host)
    linhaExecucao = os.system("ping " + ping_str + " " + host)
    retornoPing = linhaExecucao
    return retornoPing

#-------
retornoPing = 0

host = input("Digite o ip ou Endereco: ")
#www.globo.com
print("--- Comecando a pingar: ", host)
retornoPing = ping(host)
if (retornoPing == 0):
    print("------ HOST Ativo")
else:
    print("------ OPS ---- Inativo")

# lista = []
# linha1 = "64 bytes from 186.192.81.5: icmp_seq=0 ttl=243 time=43.237 ms"
# lista = linha1.split()
# print(lista)
# ipHost = lista[3][:-1]
# print("Ip do Host: " + ipHost)
# tempoResposta = lista[6][5:]
# print("Media Tempo de Resposta: " + tempoResposta)